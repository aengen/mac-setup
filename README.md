# mac-setup



## This is a basic setup for our Macs at work

I am testing this project to pre-configure a Mac with apps and settings to reduce the time taken for setup.

## I've borrowed several elements from [Jeff Geerling's Mac Development Ansible Playbook](https://github.com/geerlingguy/mac-dev-playbook)
